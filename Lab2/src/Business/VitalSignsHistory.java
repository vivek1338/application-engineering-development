/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Vivek
 */
public class VitalSignsHistory {

    private ArrayList<VitalSigns> vitalSignsHistory;
    private ArrayList<VitalSigns> abnormalList;

    public VitalSignsHistory() {
        vitalSignsHistory = new ArrayList<VitalSigns>();
    }
        
    public ArrayList<VitalSigns> getAbnormalList(double max, double min) {
        ArrayList<VitalSigns> list = new ArrayList<VitalSigns>();
        for (VitalSigns ci : this.getVitalSignsHistory()) {
            if (ci.getBlood_Pressure() < min || ci.getBlood_Pressure() > max) {
                list.add(ci);
            }
        }
        return list;
    }

    public void setAbnormalList(ArrayList<VitalSigns> abnormalList) {
        this.abnormalList = abnormalList;
    }



    public ArrayList<VitalSigns> getVitalSignsHistory() {
        return vitalSignsHistory;
    }

    public void setVitalSignsHistory(ArrayList<VitalSigns> vitalSignsHistory) {
        this.vitalSignsHistory = vitalSignsHistory;
    }

    // method has return type of carinfo
    public VitalSigns addVitalSignsInfo() {
        VitalSigns c = new VitalSigns();
        vitalSignsHistory.add(c);
        return c;
    }

    public void deleteVitalSignsInfo(VitalSigns c) {
        vitalSignsHistory.remove(c);
    }
}
