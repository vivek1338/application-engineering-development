/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Employee.Employee;
import Business.Organization.AdminOrganization;
import Business.Organization.DoctorOrganization;
import Business.Organization.LabOrganization;
import Business.UserAccount.UserAccount;

/**
 *
 * @author ran
 */
public class ConfigureABusiness {
    
    public static Business configure(){
        // Three roles: LabAssistant, Doctor, Admin
        
        Business business = Business.getInstance();
        AdminOrganization adminOrganization= new AdminOrganization();
        business.getOrganizationDirectory().getOrganizationList().add(adminOrganization);
        
        Employee employee= new Employee();
        employee.setName(" Vivek");
        
        UserAccount userAccount = new UserAccount();
        userAccount.setUsername("vivek");
        userAccount.setPassword("vivek");
        userAccount.setRole("Admin");
        userAccount.setEmployee(employee);
        
        adminOrganization.getEmployeeDirectory().getEmployeeList().add(employee);
        adminOrganization.getUserAccountDirectory().getUserAccountList().add(userAccount);
        
        
        DoctorOrganization doctorOrganization = new DoctorOrganization();
        business.getOrganizationDirectory().getOrganizationList().add(doctorOrganization);
        Employee employee1 = new Employee();
        employee1.setName(" John Doe");
        
        UserAccount account1 = new UserAccount();
        account1.setUsername("doctor");
        account1.setPassword("doctor");
        account1.setRole("Doctor");
        account1.setEmployee(employee1);
        
        doctorOrganization.getEmployeeDirectory().getEmployeeList().add(employee1);
        doctorOrganization.getUserAccountDirectory().getUserAccountList().add(account1);
        
        LabOrganization labOrganization = new LabOrganization();
        business.getOrganizationDirectory().getOrganizationList().add(labOrganization);
        Employee employee2 = new Employee();
        employee2.setName(" John Wick");
        
        UserAccount account2 = new UserAccount();
        account2.setUsername("lab");
        account2.setPassword("lab");
        account2.setRole("Lab Assistant");
        account2.setEmployee(employee2);
        
        doctorOrganization.getEmployeeDirectory().getEmployeeList().add(employee2);
        doctorOrganization.getUserAccountDirectory().getUserAccountList().add(account2);
        
        
        return business;
    }
    
}
