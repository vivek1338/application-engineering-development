/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Car;

import javax.swing.JLabel;

/**
 *
 * @author Vivek
 */
public class CarInfo {

    private String name;
    private String serialNumber;
    private String color;
    private String power;
    private String transmission;
    private String mileage;
    private String cylinderCount;
    private String speed;
    private String engine;
    private String torque;
    private String dimensions;
    private String avail;
    private String abs;
    private String seats;
    private JLabel carImage;
    private String tyreType;

    public String getTyreType() {
        return tyreType;
    }

    public void setTyreType(String tyreType) {
        this.tyreType = tyreType;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public String getTorque() {
        return torque;
    }

    public void setTorque(String torque) {
        this.torque = torque;
    }

    public String getDimensions() {
        return dimensions;
    }

    public void setDimensions(String dimensions) {
        this.dimensions = dimensions;
    }

    public String getAvail() {
        return avail;
    }

    public void setAvail(String avail) {
        this.avail = avail;
    }

    public String getAbs() {
        return abs;
    }

    public void setAbs(String abs) {
        this.abs = abs;
    }

    public String getSeats() {
        return seats;
    }

    public void setSeats(String seats) {
        this.seats = seats;
    }


    public void setCarImage(JLabel carImage) {
        this.carImage = carImage;
    }

    public JLabel getCarImage() {
        return carImage;
    }
    
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setPower(String power) {
        this.power = power;
    }

    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }

    public void setMileage(String mileage) {
        this.mileage = mileage;
    }

    public void setCylinderCount(String cylinderCount) {
        this.cylinderCount = cylinderCount;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public String getColor() {
        return color;
    }

    public String getPower() {
        return power;
    }

    public String getTransmission() {
        return transmission;
    }

    public String getMileage() {
        return mileage;
    }

    public String getCylinderCount() {
        return cylinderCount;
    }
      
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
}
