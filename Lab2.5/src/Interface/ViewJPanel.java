/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Business.VitalSigns;
import Business.VitalSignsHistory;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Vivek
 */
public class ViewJPanel extends javax.swing.JPanel {

    /**
     * Creates new form ViewJPanel
     */
    private VitalSignsHistory cih;

    public ViewJPanel(VitalSignsHistory cih) {
        initComponents();
        this.cih = cih;
        viewTable();
    }

    public void viewTable() {
        DefaultTableModel def = (DefaultTableModel) tableVitalSigns.getModel();
        def.setRowCount(0);

        for (VitalSigns ci : cih.getVitalSignsHistory()) {

            Object row[] = new Object[2];
            row[0] = ci;
            row[1] = ci.getBlood_Pressure();
            def.addRow(row);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tableVitalSigns = new javax.swing.JTable();
        detailsButton = new javax.swing.JButton();
        deleteButton = new javax.swing.JButton();
        updateButton = new javax.swing.JButton();
        confirmButton = new javax.swing.JButton();
        bpLabel = new javax.swing.JLabel();
        dateLabel = new javax.swing.JLabel();
        bpTextInputView = new javax.swing.JTextField();
        pulseTextInputView = new javax.swing.JTextField();
        dateTextInputView = new javax.swing.JTextField();
        tempLabel = new javax.swing.JLabel();
        tempTextInputView = new javax.swing.JTextField();
        pulseLabel = new javax.swing.JLabel();

        tableVitalSigns.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Blood Pressure"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tableVitalSigns);

        detailsButton.setText("Details");
        detailsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                detailsButtonActionPerformed(evt);
            }
        });

        deleteButton.setText("Delete");
        deleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteButtonActionPerformed(evt);
            }
        });

        updateButton.setText("Update");
        updateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateButtonActionPerformed(evt);
            }
        });

        confirmButton.setText("Confirm");
        confirmButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confirmButtonActionPerformed(evt);
            }
        });

        bpLabel.setText("Blood Pressure");

        dateLabel.setText("Date");

        tempLabel.setText("Temperature");

        pulseLabel.setText("Pulse");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(138, 138, 138)
                        .addComponent(detailsButton)
                        .addGap(40, 40, 40)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(confirmButton, javax.swing.GroupLayout.DEFAULT_SIZE, 81, Short.MAX_VALUE)
                            .addComponent(updateButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(40, 40, 40)
                        .addComponent(deleteButton))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(75, 75, 75)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(dateLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(106, 106, 106)
                                        .addComponent(dateTextInputView, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(pulseLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(106, 106, 106)
                                        .addComponent(pulseTextInputView, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(bpLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(106, 106, 106)
                                        .addComponent(bpTextInputView, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(tempLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(106, 106, 106)
                                        .addComponent(tempTextInputView, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(170, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(56, 56, 56)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(detailsButton)
                    .addComponent(deleteButton)
                    .addComponent(updateButton))
                .addGap(26, 26, 26)
                .addComponent(confirmButton)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tempLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tempTextInputView, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bpLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bpTextInputView, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pulseLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pulseTextInputView, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dateLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dateTextInputView, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(167, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void detailsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_detailsButtonActionPerformed
        // TODO add your handling code here:
        int selectedROw = tableVitalSigns.getSelectedRow();
        if (selectedROw >= 0) {

            VitalSigns c = (VitalSigns) tableVitalSigns.getValueAt(selectedROw, 0);
            tempTextInputView.setText(String.valueOf(c.getTemperature()));
            bpTextInputView.setText(String.valueOf(c.getBlood_Pressure()));
            dateTextInputView.setText(c.getDate());
            pulseTextInputView.setText(String.valueOf(c.getPulse()));
        } else {
            JOptionPane.showMessageDialog(null, "please select a row");
        }
    }//GEN-LAST:event_detailsButtonActionPerformed

    private void deleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteButtonActionPerformed
        // TODO add your handling code here:
        int selectedROw = tableVitalSigns.getSelectedRow();
        if (selectedROw >= 0) {
            VitalSigns c = (VitalSigns) tableVitalSigns.getValueAt(selectedROw, 0);
            cih.deleteVitalSignsInfo(c);
            JOptionPane.showMessageDialog(null, "deleted");
            viewTable();
            tempTextInputView.setText("");
            dateTextInputView.setText("");
            pulseTextInputView.setText("");
            bpTextInputView.setText("");

        } else {
            JOptionPane.showMessageDialog(null, "please select a row");
        }
    }//GEN-LAST:event_deleteButtonActionPerformed
    private void setFieldEnabled(boolean b) {
        tempTextInputView.setEnabled(b);
        dateTextInputView.setEnabled(b);
        pulseTextInputView.setEnabled(b);
        bpTextInputView.setEnabled(b);
    }

    private void updateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateButtonActionPerformed
        // TODO add your handling code here:

        int selectedROw = tableVitalSigns.getSelectedRow();
        if (selectedROw >= 0) {
            setFieldEnabled(true);
            confirmButton.setEnabled(true);
            VitalSigns c = (VitalSigns) tableVitalSigns.getValueAt(selectedROw, 0);
            tempTextInputView.setText(c.getTemperature() + "");
            dateTextInputView.setText(c.getDate() + "");
            pulseTextInputView.setText(c.getPulse() + "");
            bpTextInputView.setText(c.getBlood_Pressure() + "");
        } else {
            JOptionPane.showMessageDialog(null, "please select a row");
        }
    }//GEN-LAST:event_updateButtonActionPerformed

    private void confirmButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_confirmButtonActionPerformed
        // TODO add your handling code here:
        try{
        confirmButton.setEnabled(true);
        int selectedROw = tableVitalSigns.getSelectedRow();
        if (selectedROw >= 0) {
            VitalSigns c = (VitalSigns) tableVitalSigns.getValueAt(selectedROw, 0);
            c.setTemperature(Double.parseDouble(tempTextInputView.getText()));
            c.setBlood_Pressure(Double.parseDouble(bpTextInputView.getText()));
            c.setDate(dateTextInputView.getText());
            c.setPulse(Integer.parseInt(pulseTextInputView.getText()));
            viewTable();
            setFieldEnabled(true);
            confirmButton.setEnabled(false);
            tempTextInputView.setText("");
            bpTextInputView.setText("");
            dateTextInputView.setText("");
            pulseTextInputView.setText("");
        } else {
            JOptionPane.showMessageDialog(null, "successfully updated");
        }
        }
        catch(NumberFormatException e){
        JOptionPane.showMessageDialog(null, e+"\nPlease insert correct datatype");
        }
    }//GEN-LAST:event_confirmButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel bpLabel;
    private javax.swing.JTextField bpTextInputView;
    private javax.swing.JButton confirmButton;
    private javax.swing.JLabel dateLabel;
    private javax.swing.JTextField dateTextInputView;
    private javax.swing.JButton deleteButton;
    private javax.swing.JButton detailsButton;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel pulseLabel;
    private javax.swing.JTextField pulseTextInputView;
    private javax.swing.JTable tableVitalSigns;
    private javax.swing.JLabel tempLabel;
    private javax.swing.JTextField tempTextInputView;
    private javax.swing.JButton updateButton;
    // End of variables declaration//GEN-END:variables
}
