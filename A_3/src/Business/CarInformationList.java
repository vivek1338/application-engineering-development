/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Vivek
 */
public class CarInformationList {
private ArrayList<Car> carList;
       
        
private Timestamp updatedOn;

    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }
    
    public CarInformationList() {
        this.carList = new ArrayList<Car>();
        
        Car car1 = new Car(false, "Ferrari", 2019, 1, 2, 1,"Red One", "Boston", false);
        Car car2 = new Car(false, "BMW", 2019, 1, 4, 2, "X1", "New York", true);
        Car car3 = new Car(false, "Toyota", 2018, 1, 4, 3, "T1", "Boston", true);
        Car car4 = new Car(true, "GM", 2018, 1, 4, 4, "G1", "New York", false);
        Car car5 = new Car(true, "Toyota", 2017, 1, 4, 5, "T2", "Boston", false);
        Car car6 = new Car(false, "GM", 2017, 1, 4, 6, "G2", "Chicago", true);
        Car car7 = new Car(true, "Toyota", 2016, 1, 4, 7, "", "Seattle", false);
        Car car8 = new Car(true, "BMW", 2016, 1, 4, 8, "X2", "Seattle", true);
        Car car9 = new Car(false, "Toyota", 2019, 1, 4, 9, "T3", "Austin", false);
        Car car10 = new Car(true, "GM", 2019, 1, 4, 10, "G3", "Austin", true);
        Car car11 = new Car(true, "GM", 2019, 1, 4, 11, "G2", "Chicago", true);
        Car car12 = new Car(true, "Toyota", 2019, 1, 4, 12, "G3", "Boston", true);
        Car car13 = new Car(true, "BMW", 2019, 1, 4, 13, "G6", "Austin", true);
        Car car14 = new Car(true, "GM", 2019, 1, 4, 14, "G3", "Boston", true);
        Car car15 = new Car(true, "BMW", 2019, 1, 4, 15, "G3", "Austin", true);
        
        
        
        
        carList.add(car1);
        carList.add(car2);
        carList.add(car3);
        carList.add(car4);
        carList.add(car5);
        carList.add(car6);
        carList.add(car7);
        carList.add(car8);
        carList.add(car9);
        carList.add(car10);
        carList.add(car11);
        carList.add(car12);
        carList.add(car14);
        carList.add(car13);
        carList.add(car15);
        
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        this.updatedOn = timestamp;
    }
    
    public ArrayList<Car> getCarList() {
        return carList;
    }

    public void setCarList(ArrayList<Car> carList) {
        this.carList = carList;
    }
    
    public Car addCar(){
        Car car = new Car();
        carList.add(car);
        return car;
    }
    
    public void deleteCar(Car car){
        carList.remove(car);
    }
    
    public Car searchCarSerialNumber(Integer serialNumber){
     for(Car car:carList){
            if(car.getSerial_num()==(serialNumber)){
            return car;
            }
        }
        return null;
    }
    
    public ArrayList<Car> searchCarBrand(String brand,ArrayList<Car> brandList){
        ArrayList<Car> temp = new ArrayList<>();
     for(Car car:brandList){
            if(car.getBrand().equalsIgnoreCase(brand)){
             temp.add(car);
            }
        }
        return temp;
    }
    
     public ArrayList<Car> searchCarMfg(Integer mfgYear,ArrayList<Car> mfgList){
     ArrayList<Car> temp = new ArrayList<>();
     for(Car car:mfgList){
            if(car.getManufactured_year()== mfgYear){
             temp.add(car);
            }
        }
        return temp;
    }
     
     public ArrayList<Car> searchCarCity(String city,ArrayList<Car> cityList){
     ArrayList<Car> temp = new ArrayList<>();
     for(Car car:cityList){
            if(car.getAvailble_city().equalsIgnoreCase(city)){
             temp.add(car);
            }
        }
        return temp;
    }
     
     public ArrayList<Car> searchModelNumber(String modelNumber,ArrayList<Car> modelNumberList){
     ArrayList<Car> temp = new ArrayList<>();
     for(Car car:modelNumberList){
            if(car.getModel_num().equalsIgnoreCase(modelNumber)){
             temp.add(car);
            }
        }
        return temp;
    }
     
     public ArrayList<Car> searchSeatsCar(Integer minSeats,Integer maxSeats,ArrayList<Car> seatsSearchList){
     ArrayList<Car> temp = new ArrayList<>();
     for(Car car:seatsSearchList){
         if(car.getMin_seats() >= minSeats){
            if(car.getMax_seats() <= maxSeats){                
             temp.add(car);
                }
            }
        }
        return temp;
    }
         
    public ArrayList<Car> searchExpiredCarList(ArrayList<Car> modelNumberList){
     ArrayList<Car> temp = new ArrayList<>();
     for(Car car:modelNumberList){
            if(car.isMaintenance_certificate()==false){
                temp.add(car);
            }
        }
        return temp;
    }
}
